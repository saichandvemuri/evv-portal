var gulp = require('gulp');

var sass = require('gulp-sass'); // important gulp pulgins
rename = require("gulp-rename");
var minify = require('gulp-minify-css');
var browserSync = require("browser-sync").create();

// gulp sass task

gulp.task('sass', function () {
  return gulp.src('assets/saas/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('assets/css'))
    .pipe(rename({ suffix: ".min" }))
    .pipe(minify())
    .pipe(gulp.dest('assets/css'))
    .pipe(browserSync.reload({
      stream : true
    }));

});


gulp.task('watch', function () {
  gulp.watch('assets/saas/*.scss', gulp.series('sass'));
  gulp.watch("*.html").on('change', browserSync.reload);
});

gulp.task('browserSync', function () {
  browserSync.init({
    server: "./"
  });
  gulp.watch("assets/saas/*.scss").on('change', browserSync.reload);
  gulp.watch('assets/js/*.js').on('change', browserSync.reload);
  gulp.watch("*.html").on('change', browserSync.reload);
});

gulp.task('default', gulp.parallel('watch', 'sass', 'browserSync'));